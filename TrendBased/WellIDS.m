function ids = WellIDS(DataCell , RefWellId , NumMems)


[NumSeries] = numel(DataCell);
Similarity  = zeros(NumSeries,1);
for i=1:NumSeries
        
        series1 = DataCell{RefWellId}(:);
        series2 = DataCell{i}(:);
        
        window  = 2;                                                        %!!
        Similarity(i) = dtw(series1,series2,window);                       % DTW mex64 measure could be anything else but be careful of different sizes of series

end

[~ , indx] = sort(Similarity , 'descend'); % sort the similarity 
ids        = indx(1:NumMems-1);

end