% clustering with linear trends
clc;clear;close all;
%% --------------- installation CVX
% addpath(genpath('..\cvx-w64\cvx'));cvx_setup

%% -------------- Read Data
load('../data/data1100AllFeatures.mat')
oil    = oil./days;
zOil   = (oil==0);
oil    = log(oil+1);
% oil(zOil) = 0;

oil(5,:) = []; % wiered



%% ------------------ get the trend
DesiredAtt = oil;

%----------
% get the true length
%----------
lens =  Truelength (DesiredAtt);
ids  = find(lens > 100); % only considers series with larger than 100 length %!!



for i=1:length(ids)
    
    y = DesiredAtt(ids(i),:); % selected well with length larger than 100
    
    
    
    %------------
    % preprocess
    %------------
    y = y(1:lens(ids(i)))'; % remove the end zeros in the series  due to catastrophic failures
    y(isnan(y)) = 0; % remove NaNs
    
    if length(find(y==0))==length(y); continue; end % dont process the wells with no data or all zero values

    
    %------------------------
    % filtering
    %------------------------
%     lambda_max = 50;
    lambda_max   = l1tf_lambdamax(y)/10;                                    %!!
    sC           = 1e-3;                                                    %!!
    
    [x, knots, pp] = l1tfRobust(y, lambda_max, sC);
    slope = pp(1,:);
    
    
    Out.TimeSeries(i)={y(:)};
    Out.Trends(i)  = {x(:)};
    Out.Points(i)  = {pp};
    Out.knots(i)   = {knots};
    Out.Slopes(i)  = {slope};
    
    %-------------------
    % visualization
    %-------------------
%     figure(1);
%     subplot(4, 4, i)
%     hold on
%     plot(y, 'b', 'LineWidth', 2);
%     plot(x, 'r', 'LineWidth', 2);
%     xlim([1, lens(ids(i))])
%     box on
%     disp(i)
    %------------------
end
save OutTrend Out DesiredAtt 












