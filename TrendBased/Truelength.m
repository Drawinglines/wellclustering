function lens =  Truelength(y)


% Compute the length of each time series
[N , T] = size(y);
lens = T - sum(isnan(y), 2);
for i = 1:N
    if lens(i) > 0
        while y(i,lens(i))==0
            lens(i) = lens(i)-1;
            if lens(i) == 0
                break;
            end
        end
    end
end




end