%% --------------- Clustering algorithms
clear;clc;close all;
%%
load OutTrend
% DesiredClus = Out.TimeSeries; % the desired set of time series to be clustered
DesiredClus = Out.Slopes;


% ElemsInCluster = 20;                                                        %!!
% %---------------------
% % Dynamic Time Warping
% %---------------------
% window  = 2;                                                                %!!
% funName = 'dtw';
% 
% NumSeries = numel(DesiredClus);
% W = zeros(NumSeries,NumSeries);
% indx = [];
% for i=1:NumSeries
%     for j=1:NumSeries
%         
%         s = DesiredClus{i}(:);
%         t = DesiredClus{j}(:);
%         
%         % W(i,j) = s'*t;
%         W(i,j) = mdtw(s,t,window); % DTW mex64 measure could be anything else but be careful of different sizes of series
% %         W(i,j) = dtw(s,t,window); % DTW measure could be anything else but be careful of different sizes of series
% 
%     end
% end
% 
% numClus   = 100;
% Type      = 2;
% %      1 - Unnormalized
% %      2 - Normalized according to Shi and Malik (2000)
% %      3 - Normalized according to Jordan and Weiss (2002)
% 
% [C, L, U] = SpectralClustering(W, numClus, Type);



%% Gives cluster memebers for a given well id number of memebers are NumMem
NumMems   = 20;
RefWellId = 292;

ids       = WellIDS(DesiredClus , RefWellId , NumMems);


% -------------------
% visualization
% -------------------
figure(1);
for i = 1:NumMems
    subplot(4, 5, i)
    plot(Out.TimeSeries{i}, 'b', 'LineWidth', 2);
end
% ------------------




