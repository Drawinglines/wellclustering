% Load wor, oil, days, water, and names by importing
% ID      PadID   PoolID  Xpos    Ypos    days    nData   oil     type    water   wor     year   
clc

pids = unique(PoolID);
N = length(type);
pool = zeros(N, length(pids));
for i = 1:N
    pool(i, ismember(pids, PoolID{i})) = 1;
end

Role = zeros(N, 1);
Role(ismember(type, 'P')) = 1;

clear type i N

save('data4.mat')