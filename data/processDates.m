clc
clear

load data4.mat

dates = date{:,:};

yy = cell(size(dates, 1), 1); mm = cell(size(dates, 1), 1);
yymin = 2016; 
for i = 1:size(dates, 1)
    yy{i} = 0;
    for j = 1:size(dates, 2)
        if strcmp(dates{i,j}, '')
            break
        else
            if i == 1067
                CC = strsplit(dates{i,j},'/');
                yy{i}(j) = str2num(CC{3});
                mm{i}(j) = str2num(CC{1});
            else
                CC = strsplit(dates{i,j},'-');
                yy{i}(j) = str2num(CC{1});
                mm{i}(j) = str2num(CC{2});
            end
        end
    end
    yymin = min([yymin,yy{i}]);
    fprintf('%d:\t %d\n', i, yymin)
end

nDates = cell(size(dates, 1), 1);
for i = 1:length(nDates)
    nDates{i} = (12*(yy{i}-yymin) + mm{i})';
end

clear date N yy mm i j dates
save('data4p.mat')
