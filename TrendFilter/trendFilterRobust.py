#!/usr/bin/python
from numpy.linalg import norm, solve
from scipy.optimize import linprog
import numpy as np
import csv


def lambdaMax(y):
    T = len(y)
    I = np.eye(T-2)
    O = np.zeros((T-2,1))
    D = np.concatenate([I,O,O], axis=1)\
        + np.concatenate([O,-2*I,O], axis=1)\
        + np.concatenate([O,O,I], axis=1)
    return norm(solve(np.dot(D,D.transpose()), D*y), np.inf)

def robustL1trendFilter(y, lmbd):
    lam = lambdaMax(y)*lmbd
    print lam
    T = len(y)
    # Create a tridiagonal
    D = np.concatenate( [np.diag(np.ones((T-2,))), np.zeros((T-2,2))], axis=1) \
        + np.concatenate( [np.zeros((T-2,1)), np.diag(-2*np.ones((T-2,))), np.zeros((T-2,1))], axis=1) \
        + np.concatenate( [np.zeros((T-2,2)), np.diag(np.ones((T-2,)))], axis=1)
    Z = np.diag(1.0*(y!=0))

    # Form the linear program
    A = np.zeros((4*T-4,3*T-2))
    c = np.zeros((3*T-2,))
    b = np.zeros((4*T-4,))

    # Fill A matrix
    A[0:T-2,0:T] = D
    A[T-2:2*T-4,0:T] = -D
    A[0:T-2, T:2*T-2] = np.diag(-np.ones((T-2,)))
    A[T-2:2*T-4, T:2*T-2] = np.diag(-np.ones((T-2,)))
    A[2*T-4:3*T-4, 0:T] = Z
    A[3*T-4:4*T-4, 0:T] = -Z
    A[2*T-4:3*T-4, 2*T-2:3*T-2] = np.diag(-np.ones((T,)))
    A[3*T-4:4*T-4, 2*T-2:3*T-2] = np.diag(-np.ones((T,)))

    # Fill c vector
    c[T:2*T-2] = lam
    c[2*T-2:3*T-2] = 1

    # Fill b vector
    b[2*T-4:3*T-4] = y
    b[3*T-4:4*T-4] = -y

    print (np.min(y), np.max(y))
    sol = linprog(c, A_ub=A, b_ub=b, bounds=(np.min(y),np.max(y)))
    return sol.x[0:T]
    
# Import the data
def getData():
    reader=csv.reader(open("../../datacleansing/data/oil4.csv","rb"),delimiter=',')
    oil = [np.array(line).astype(int) for line in reader]

    reader=csv.reader(open("../../datacleansing/data/days4.csv","rb"),delimiter=',')
    days = [np.array(line).astype(int) for line in reader] 

    # Pre-process the data
    lens = np.zeros((len(oil),1),dtype=np.int)
    for i in range(0, len(oil)):
        if np.sum(oil[i]) == 0:
            oil[i] = []
        else:
            oil[i] = oil[i]/days[i]
            zOil = oil[i] == 0
            oil[i] = np.log10(oil[i])
            oil[i][zOil] = 0
        lens[i] = len(oil[i])
    return (oil, lens)

## Main Function
oil, lens = getData()

print oil[1]

robustL1trendFilter(oil[1], 0.1)

