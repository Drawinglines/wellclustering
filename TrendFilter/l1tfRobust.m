function [x, knots, pp] = l1tfRobust(y, lambda, sC)
T = length(y);
D = full(gallery('tridiag',T,-1,2,-1));
D(1, :) = [];  D(end, :) = [];
Z = diag(1.0*(y~=0));

% sC = 1e-2;

cvx_begin quiet
    variable x(T)
    minimize(norm(Z*x - y, 1) + lambda*norm(D*x,1) + sC*sum_square(x))
cvx_end


[knots, pp] = cleanx(x, D);

% Reconstruction
kn = [1; knots; length(x)];
z = 0*x;
for i = 1:length(kn)-1
    z(kn(i):kn(i+1)) = polyval(pp(:, i)', kn(i):kn(i+1))';
end
% figure
% subplot(2, 1, 1)
% hold on
% plot(y)
% plot(x, 'r')
% plot(z, 'g')
% subplot(2, 1, 2)
% stem(2:length(x)-1, D*x)

end

function [knots, pp] = cleanx(x, D)
th = 0.1;
z = abs(D*x);
z(z < th*mean(z)) = 0;

% Combine neighbors
for i = length(z):-1:2
    if (z(i)>0)&&(z(i-1)>0)
        z(i-1) = z(i-1)+z(i);
        z(i-1) = 0;
    end
end

% Delete the very small jumps
z(z < th*mean(z)) = 0;

% Fit linear regression in between knots
knots = find(z > 0) + 1;
pp = zeros(2, length(knots)+1);
kn = [1; knots; length(x)];
for i = 2:length(kn)
    pp(:, i-1) = polyfit(kn(i-1):kn(i), x(kn(i-1):kn(i))',1)';
end

end