% Trend Filtering
clc;close all;
clear

addpath(genpath('..\cvx-w64\cvx'));cvx_setup
load('../data/data1100AllFeatures.mat')

Lambda = 0.1;
oil    = oil./days;
zOil   = (oil==0);
oil    = log10(oil);
oil(zOil) = 0;

% Compute the length of each time series
[N, T] = size(oil);
lens = T - sum(isnan(oil), 2);
for i = 1:N
    if lens(i) > 0
        while oil(i, lens(i)) == 0
            lens(i) = lens(i) -1;
            if lens(i) == 0
                break
            end
        end
    end
end

ids = find(lens > 100);
% ids(17:end) = [];
for i = 1:length(ids)
    subplot(4, 4, i)
    y = oil(ids(i), 1:lens(ids(i)))';
    y(isnan(y)) = 0;
    hold on
    plot(y, 'b', 'LineWidth', 2)
    lambda_max = l1tf_lambdamax(y);
    z = l1tfRobust_Reza(y, Lambda*lambda_max);
    plot(z, 'r', 'LineWidth', 2)
    xlim([1, lens(ids(i))])
    box on
    disp(i)
end
