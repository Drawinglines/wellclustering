function x = l1tfRobust_Reza(y, lambda)
T = length(y);
D = full(gallery('tridiag',T,-1,2,-1));
D(1, :) = [];  D(end, :) = []; D = -D;
Z = diag(1.0*(y~=0));

sC = 1e-3;

cvx_begin quiet
    variable x(T)
    minimize(0.5*norm(Z*x - y, 1) + lambda*norm(D*x,1) + sC*sum_square(x))
cvx_end

end
