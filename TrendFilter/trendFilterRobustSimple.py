#!/usr/bin/python
from numpy.linalg import norm, solve
from scipy.optimize import linprog
import matplotlib.pyplot as plt
import numpy as np
import csv

def lambdaMax(y):
    """ This function find the smallest lambda that makes the piecewise approximation
    a single line.
    """
    T = y.shape[0]
    I = np.eye(T-2)
    O = np.zeros((T-2,1))
    D = np.concatenate([I,O,O], axis=1)\
        + np.concatenate([O,-2*I,O], axis=1)\
        + np.concatenate([O,O,I], axis=1)
    return norm(solve(np.dot(D,D.transpose()), D*y), np.inf)
    
def tsSegment(x, D):
    """ This function computes the indexes of x in which a change point has happened.
    """
    z = np.abs(np.dot(D, x))        # Taking the second order difference of x
    th = 0.1
    z[ z < th*np.mean(z) ] = 0      # Removing the very small changes
    
    knots = np.nonzero(z) + 1
    
    return z

def robustL1trendFilter(y, lmbd):
    ''' This function find a piecewise linear trend for the input.
    Inputs: y: the original (N,) time series;  lmbd: the regularization parameter.
    Outputs: x: the trend;  segs: the change points in the time series.
    '''
    lam = lambdaMax(y)*lmbd
    T = y.shape[0]
    # Create a tridiagonal
    D = np.concatenate( [np.diag(np.ones((T-2,))), np.zeros((T-2,2))], axis=1) \
        + np.concatenate( [np.zeros((T-2,1)), np.diag(-2*np.ones((T-2,))), np.zeros((T-2,1))], axis=1) \
        + np.concatenate( [np.zeros((T-2,2)), np.diag(np.ones((T-2,)))], axis=1)
    Z = np.diag(1.0*(y!=0))

    # Form the linear program
    A = np.zeros((4*T-4,3*T-2))
    c = np.zeros((3*T-2,))
    b = np.zeros((4*T-4,))

    # Fill A matrix
    A[0:T-2,0:T] = D
    A[T-2:2*T-4,0:T] = -D
    A[0:T-2, T:2*T-2] = np.diag(-np.ones((T-2,)))
    A[T-2:2*T-4, T:2*T-2] = np.diag(-np.ones((T-2,)))
    A[2*T-4:3*T-4, 0:T] = Z
    A[3*T-4:4*T-4, 0:T] = -Z
    A[2*T-4:3*T-4, 2*T-2:3*T-2] = np.diag(-np.ones((T,)))
    A[3*T-4:4*T-4, 2*T-2:3*T-2] = np.diag(-np.ones((T,)))

    # Fill c vector
    c[T:2*T-2] = lam
    c[2*T-2:3*T-2] = 1

    # Fill b vector
    b[2*T-4:3*T-4] = y
    b[3*T-4:4*T-4] = -y

    #print (np.min(y), np.max(y))
    sol = linprog(c, A_ub=A, b_ub=b, bounds=(np.min(y),np.max(y)))
    
    # Now segment the time series
    segs = tsSegment(sol.x[0:T], D)
    
    return sol.x[0:T], segs

## Main Function
oil = np.random.standard_normal((20,))
#print type(oil), oil.shape[0]
#print oil

x =  robustL1trendFilter(oil, 0.1)

plt.plot(np.arange(0,len(oil)), oil)
plt.plot(np.arange(0,len(x)), x, 'r')
plt.show()